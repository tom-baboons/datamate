const { GenerateSW } = require("workbox-webpack-plugin");

module.exports = {
  publicPath: process.env.NODE_ENV === "development" ? "/" : "",

  configureWebpack: {
    plugins: [new GenerateSW()]
  },
  "transpileDependencies": [
    "vuetify"
  ],
    pwa: {
      name: 'DataMate',
      themeColor: '#CBE9BA',
      msTileColor: '#CBE9BA',
      appleMobileWebAppCapable: 'yes',
      appleMobileWebAppStatusBarStyle: '#191919C'
      
  
    } 
};