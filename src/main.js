import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import VueCurrencyInput from './plugins/currency'
import './registerServiceWorker'


Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  VueCurrencyInput,
  render: h => h(App)
}).$mount('#app')
